import logo from "./logo.svg";
import "./App.css";
import Header from "./BaiTapLayoutComponent/Header";
import Body from "./BaiTapLayoutComponent/Body";
import Footer from "./BaiTapLayoutComponent/Footer";
function App() {
  return (
    <div className="App">
      <Header />
      <Body />
      <Footer />
    </div>
  );
}

export default App;
